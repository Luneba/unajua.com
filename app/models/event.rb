class Event < ApplicationRecord

	belongs_to :user
	validates :user_id, presence: true

	validates :image, presence: true

  validates :address, presence: true

  has_attached_file :image, styles: { :medium => "450x300" }
  validates :image, length: { minimum: 3, maximum: 300 }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  has_many :comments, dependent: :destroy

  scope :of_followed_users, -> (following_users) { where user_id: following_users }
	#Likes
	acts_as_votable
	#Notifications
  has_many :notifications, dependent: :destroy

  has_many :passive_attends, class_name: "Attend", foreign_key: "attended_event_id", dependent: :destroy
  has_many :attendees, through: :passive_attends, source: :attendee

  #Maps
  after_validation :exclude_united_states_text_from_address
  geocoded_by :address
  after_validation :geocode, :if => :address_changed?

  private

    def exclude_united_states_text_from_address
      self.address = address.gsub!(/(united states)/i, " ").strip!.chomp!(",") if address.downcase.include?("united states")
    end

end
