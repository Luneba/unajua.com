class User < ApplicationRecord


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username, presence: true, length: { minimum: 4, maximum: 16 }

  has_many :events, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :active_attends, class_name: "Attend", foreign_key: "attendee_id", dependent: :destroy
  has_many :attending, through: :active_attends, source: :attended_event
  
  has_attached_file :avatar, styles: { medium: '152x152#', small: '75x75#' }
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/


	has_many :follower_relationships, foreign_key: :following_id, class_name: 'Follow'
  has_many :followers, through: :follower_relationships, source: :follower
  has_many :following_relationships, foreign_key: :follower_id, class_name: 'Follow'
  has_many :following, through: :following_relationships, source: :following

  def follow(user_id)
    following_relationships.create(following_id: user_id)
  end
  
  def unfollow(user_id)
    following_relationships.find_by(following_id: user_id).destroy
  end


  has_attached_file :avatar, styles: { medium: '152x152#' }
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  #Likes
  acts_as_voter

  #Notifications
  has_many :notifications, dependent: :destroy

  #Conversation
  has_many :messages
  has_many :conversations, foreign_key: :sender_id

  #Adds user to attend list for event
  def attend(event)
    active_attends.create(attended_event_id: event.id)
  end

  #Removes a user from the attend list for event
  def unattend(event)
    active_attends.find_by(attended_event_id: event.id).destroy
  end

  #Returns true if the current user is attending the event
  def attending?(event)
    attending.include?(event)
  end

end
