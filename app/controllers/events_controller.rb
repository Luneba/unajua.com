class EventsController < ApplicationController

	before_action :authenticate_user!
	before_action :set_event, only: [:show, :edit, :update, :destroy]
	before_action :set_event_for_nested_object, only: [:liked, :unliked]
	before_action :owned_event, only: [:edit, :update, :destroy]
	before_action :correct_user, only: [:edit, :update, :destroy]


	def index
		@events = Event.all.order('created_at DESC').page params[:page]
		# @events = Event.of_followed_users(current_user.following).order('created_at DESC').page params[:page]
	end

	def new
		@event = current_user.events.build
	end

	def create
		@event = current_user.events.build(event_params)
		if @event.save
			current_user.attend(@event)
			flash[:success] = "Your event has been created!"
			redirect_to events_path
		else
			flash.now[:alert] = "Your new event couldn't be created!  Please check the form."
			render :new
		end
	end

	def show
		@event = Event.find(params[:id])
		@hash = Gmaps4rails.build_markers(@event) do |event, marker|
      marker.lat event.latitude
      marker.lng event.longitude
    end
	end

	def edit
	end

	def update
		if @event.update(event_params)
			flash[:success] = "Event updated."
			redirect_to events_path
		else
			flash.now[:alert] = "Update failed.  Please check the form."
			render :edit
		end
	end

	def destroy
		@event.destroy
		flash[:success] = "Post deleted"
		redirect_to events_path
	end

	def liked
		@event.liked_by current_user
		create_notification @event
		respond_to do |format|
			format.html { redirect_to root_path }
			format.js
		end
	end

	def unliked
		@event.unliked_by current_user

		respond_to do |format|
			format.html { redirect_to root_path }
			format.js
		end
	end

	def attendees
    @event  = Event.find(params[:id])
    @users = @event.attendees.paginate(page: params[:page])
  end


	private

	def event_params
		params.require(:event).permit(:image, :title, :description, :date, :price, :place, :address, :latitude, :longitude)
	end

	def set_event
    @event = Event.find(params[:id])
  end

	def set_event_for_nested_object
		@event = Event.find(params[:event_id])
	end


  def owned_event
	  unless current_user == @event.user
	    flash[:alert] = "That event doesn't belong to you!"
	    redirect_to root_path
	  end
	end

	def correct_user
    unless @event.user.id == current_user.id
      flash[:alert] = "That event doesn't belong to you!"
      redirect_to root_path
    end
  end
  
  def create_notification(event)
    return if event.user == current_user
    Notification.create(user_id: event.user.id,
                        notified_by_id: current_user.id,
                        event_id: event.id,
                        identifier: event.id,
                        notice_type: 'liked')
  end

end
