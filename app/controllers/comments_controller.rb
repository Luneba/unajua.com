class CommentsController < ApplicationController

	before_action :set_event
  before_action :authenticate_user!
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]

	  def index
      @comments = @event.comments.order("created_at ASC")

      respond_to do |format|
        format.html { render :index }
      end
    end

    def new
      @comment = @event.comments.build
    end

   def create
    @comment = @event.comments.build(comment_params)
    @comment.user_id = current_user.id

    if @comment.save
      create_notification @event
      respond_to do |format|
        format.html { redirect_to root_path }
        format.js
      end
    else
      flash[:alert] = "Check the comment form, something went wrong."
      render new
    end
  end

  def show
  end

  def edit
  end

  def update
  end


	def destroy
    @comment.destroy
    flash[:success] = "Comment deleted"
    redirect_to root_path
  end


	private

  def create_notification(event)
    return if event.user.id == current_user.id
      Notification.create(user_id: event.user.id,
                          notified_by_id: current_user.id,
                          event_id: event.id,
                    			identifier: @comment.id,
                          notice_type: 'comment')
  end

	def comment_params
	  params.require(:comment).permit(:content)
	end

	def set_event
	  @event = Event.find(params[:event_id])
	end

   def set_comment
    @comment = @event.comments.find(params[:id])
  end

  def correct_user
    unless @comment.user.id == current_user.id
      flash[:alert] = "That comment doesn't belong to you!"
      redirect_to :back
    end
  end

end
