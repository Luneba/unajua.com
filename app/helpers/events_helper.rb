module EventsHelper
  def likes_helper(event, user)
    if user.voted_for?(event)
      event_unliked_url(event)
    else
      event_liked_url(event)
    end
  end

  def likes_icon(event, user)
    if user.voted_for?(event)
      "fi fi-heart full_heart"
    else
      "fi fi-heart empty_heart"
    end
  end
end
