Rails.application.routes.draw do



  resources :events do
      resources :comments
      get 'liked/', to: 'events#liked'
      get 'unliked/', to: 'events#unliked'
      member do
      get :attendees
    end
  end

  #Chat routes
  get 'chat/index'
  resources :conversations, only: [:create] do
    resources :messages, only: [:create]
    
    member do
      post :close
    end
  end

  resources :attends, only: [:create, :destroy]

  
  #Follow and Unfollow routes
  post ':username/follow_user', to: 'relationships#follow_user', as: :follow_user
  post ':username/unfollow_user', to: 'relationships#unfollow_user', as: :unfollow_user

  get 'notifications/:id/link_through', to: 'notifications#link_through',
                                        as: :link_through
  get 'notifications', to: 'notifications#index'

  get 'profiles/show'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, :controllers => { registrations: 'registrations' }


  resources :users do
    member do
      get :attending
    end
  end


  get ':username', to: 'profiles#show', as: :profile
  get ':username/edit', to: 'profiles#edit', as: :edit_profile
  patch ':username/edit', to: 'profiles#update', as: :update_profile

  root 'events#index'

end
